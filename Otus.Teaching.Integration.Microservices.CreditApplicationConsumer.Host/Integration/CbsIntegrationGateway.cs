﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.Contract;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host.Integration
{
    public class CbsIntegrationGateway
        : ICbsIntegrationGateway
    {
        public Task CreateNewCreditApplicationAsync(CreateNewCreditApplicationContract application)
        {
            Console.WriteLine($"Успешно отправили кредитную заявку с Id {application.Id} на создание в кредитную систему...");
            
            return Task.CompletedTask;;
        }
    }
}