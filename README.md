## Демо для занятия "Паттерны интеграции корпоративных приложений. Введение в микросервисы"

Описание проектов

#### Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host

Проект сервиса "Агрегатор кредитных заявок"
Имеет RPC-like API на базе HTTP и Swagger UI
Работает с очередью через Mass Transit для создания кредитной заявки
Имеет Hosted Service, имитирующий импорт кредитных заявок с ФТП или сетевой папки

Для полноценной работы сервиса нужно, чтобы был доступен брокер Rabbit Mq (можно закомментировать код регистрации MassTransit, чтобы посмотреть только API)

Брокер можно запустить в Docker через Docker-compose файл в корне репозитория

Команда:

`docker-compose up`

#### Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host
Консюмер очереди на создание кредитной заявки через Mass Transit

#### Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Host
Сервис, имитирующий экспорт кредитных заявок на ФТП или сетевую папку из CRM системы
