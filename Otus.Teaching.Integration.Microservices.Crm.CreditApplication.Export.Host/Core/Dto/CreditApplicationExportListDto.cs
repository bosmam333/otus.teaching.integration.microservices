﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto
{
    [XmlRoot("CreditApplications")]
    public class CreditApplicationExportListDto
    {
        public List<CreditApplicationExportDto> CreditApplications { get; set; }
    }
}