﻿using System.Collections.Generic;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export
{
    public interface ICreditApplicationFileSerializer
    {
        void SerializeToFile(List<CreditApplicationExportDto> customers);
    }
}