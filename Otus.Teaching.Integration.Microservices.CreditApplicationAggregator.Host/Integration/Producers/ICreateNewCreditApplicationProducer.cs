﻿using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Integration.Producers
{
    public interface ICreateNewCreditApplicationProducer
    {
        Task CreateNewCreditApplication(CreditApplicationImportDto application);
    }
}