﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.DataAccess.Repositories.Abstractions;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.DataAccess.Repositories
{
    public class FakeCreditApplicationRepository
        : ICreditApplicationRepository
    {
        public Task<IEnumerable<CreditApplication>> GetAllAsync()
        {
            return Task.FromResult(new List<CreditApplication>()
            {
                new CreditApplication()
                {
                    Id = Guid.Parse("5e331afd-0a4c-456d-82df-2ab48dc46f98"),
                    Channel = AcquisitionChannel.Street,
                    CreatedDate = DateTime.Now,
                    CustomerId = Guid.NewGuid(),
                    Status = CreditApplicationStatus.New,
                    Sum = 121212m
                }
            }.AsEnumerable());
        }
        

        public Task<CreditApplication> GetByIdAsync(Guid id)
        {
            return Task.FromResult(new CreditApplication()
            {
                Id = Guid.Parse("5e331afd-0a4c-456d-82df-2ab48dc46f98"),
                Channel = AcquisitionChannel.Email,
                CreatedDate = DateTime.Now,
                CustomerId = Guid.NewGuid(),
                Status = CreditApplicationStatus.New,
                Sum = 121212m
            });
        }

        public Task AddAsync(CreditApplication entity)
        {
            return Task.CompletedTask;
        }

        public Task UpdateAsync(CreditApplication entity)
        {
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid entityId)
        {
            return Task.CompletedTask;
        }
    }
}
