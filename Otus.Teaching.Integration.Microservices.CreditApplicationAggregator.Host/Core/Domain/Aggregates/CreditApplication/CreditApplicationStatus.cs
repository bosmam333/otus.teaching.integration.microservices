﻿namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication
{
    public enum CreditApplicationStatus
    {
        New = 1,
        InWork = 2,
        Finished = 3
    }
}