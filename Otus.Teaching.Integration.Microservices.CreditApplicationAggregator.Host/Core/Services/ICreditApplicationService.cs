﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Services
{
    public interface ICreditApplicationService
    {
        Task<CreditApplicationsForListDto> GetCreditApplicationsForListAsync();
        
        Task<CreditApplicationDto> GetCreditApplicationAsync(Guid id);
        
        Task<CreditApplicationReadyToSendDto> CreateCreditApplicationAsync(CreateCreditApplicationDto creditApplicationDto);
        
        Task CreateCreditApplicationByBusAsync(CreateCreditApplicationDto creditApplicationDto);
        
        Task EditCreditApplicationAsync(EditCreditApplicationDto creditApplicationDto);
    }
}
