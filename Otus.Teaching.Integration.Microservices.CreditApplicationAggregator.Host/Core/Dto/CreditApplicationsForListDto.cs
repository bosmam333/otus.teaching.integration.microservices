﻿using System.Collections.Generic;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    public class CreditApplicationsForListDto
    {
        public List<CreditApplicationsForListItemDto> Items { get; set; }
    }
}
